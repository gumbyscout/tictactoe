package gumbyscout.tictactoe;

import gumbyscout.tictactoe.GameState.Player;
import gumbyscout.tictactoe.GameState.Position;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.TableLayout;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class GameBoard extends TableLayout implements OnClickListener {
	private GamePiece[][] gamePieces;
	private int board_height;
	private int board_width;
	private final String LABEL_BUNDLE_STATES = "GamePieceStates";
	private Paint gameBoardPaint;
	private TextView playerIndicator;
	private AlertDialog winningBox;
	private AlertDialog resetBox;
	private GameState gamePieceStates;
	
	public GameBoard(Context context) {
		super(context);
		init();
	}

	public GameBoard(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	

	public void init() {
		// make onDraw actually work
		setWillNotDraw(false);

		// set game board dimensions, default 3x3
		board_width = 3;
		board_height = 3;

		// set paint for grid
		gameBoardPaint = new Paint();
		gameBoardPaint.setARGB(255, 0, 0, 0);
		gameBoardPaint.setStrokeWidth(8);

		// testing color
		setBackgroundColor(0xFF00FFFF);

		// winning box
		AlertDialog.Builder winningBoxBuilder = new AlertDialog.Builder(
				getContext());
		winningBoxBuilder.setCancelable(false);
		winningBoxBuilder.setPositiveButton(R.string.label_confirm,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						clearStates();
					}
				});
		winningBoxBuilder.setNegativeButton(R.string.label_quit,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
						((Activity) getContext()).finish();
					}
				});
		winningBox = winningBoxBuilder.create();
		
		// reset box
		AlertDialog.Builder resetBoxBuilder = new AlertDialog.Builder(
				getContext());
		resetBoxBuilder.setCancelable(false);
		resetBoxBuilder.setPositiveButton(R.string.label_confirm,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						clearStates();
					}
				});
		resetBoxBuilder.setNegativeButton(R.string.label_cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
					}
				});
		resetBox = resetBoxBuilder.create();
	}
	
	public void setScreenDimensions(int screen_width, int screen_height){
		// set game board visual size, assume linear layout parent
		// landscape orientation
		if (screen_width > screen_height) {
			setLayoutParams(new LinearLayout.LayoutParams(screen_height,
					screen_height));
		}
		// portrait orientation
		else if (screen_height > screen_width) {
			setLayoutParams(new LinearLayout.LayoutParams(screen_width,
					screen_width));
		}
	}

	public void populateBoard() {
		// debug
		Log.d("DEBUG", "Populating Board");

		// create the array to hold the pieces
		gamePieces = new GamePiece[board_height][board_width];

		// check for previous state array
		if (gamePieceStates != null) {

		} else {
			gamePieceStates = new GameState();
		}

		for (int i = 0; i < board_height; i++) {
			// add rows
			TableRow tr = new TableRow(getContext());
			// set layout params
			tr.setLayoutParams(new GameBoard.LayoutParams(
					GameBoard.LayoutParams.MATCH_PARENT,
					GameBoard.LayoutParams.MATCH_PARENT, 1.0f));
			for (int k = 0; k < board_width; k++) {
				// add gamepieces to rows
				GamePiece gp = new GamePiece(getContext());
				// set layout params
				gp.setLayoutParams(new TableRow.LayoutParams(
						TableRow.LayoutParams.MATCH_PARENT,
						TableRow.LayoutParams.MATCH_PARENT, 1.0f));
				// add listener
				gp.setOnClickListener(this);

				// add to array of gamepieces
				gamePieces[i][k] = gp;

				tr.addView(gp);
			}
			addView(tr);
		}
		updateBoard();
	}
	
	public void reset(){
		resetBox.setMessage(getContext().getString(R.string.dialog_reset));
		resetBox.show();
	}

	public void clearStates() {
		// this clears the states to NONE, so you can reset the game
		gamePieceStates = new GameState();
		updateBoard();
	}

	public void updateBoard() {
		for (int i = 0; i < board_height; i++) {
			for (int k = 0; k < board_width; k++) {
				gamePieces[i][k].setGamePieceState(gamePieceStates.pieces[i][k]);
				gamePieces[i][k].invalidate();
			}
		}

		updatePlayerIndicator();
	}

	public GameState getGameState() {
		return gamePieceStates;
	}

	public void setGameBoardState(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			Log.d("STATE", "There was a previous state!");
			gamePieceStates = (GameState) savedInstanceState
					.getSerializable(LABEL_BUNDLE_STATES);
			updateBoard();
		} else {
			Log.d("STATE", "Did nothing!");
		}
	}

	public void setPlayerIndicator(TextView playerInd) {
		this.playerIndicator = playerInd;
		updatePlayerIndicator();
	}

	public void playVictory() {
		// this determines the ending type
		String message = "";
		if(gamePieceStates.isWin(Player.X)){
			message = getContext().getString(R.string.win_cross);
		}
		else if (gamePieceStates.isWin(Player.O)){
			message = getContext().getString(R.string.win_circle);
		} 
		else {
			message = getContext().getString(R.string.win_tie);
		}
		message += '\n' + getContext().getString(R.string.dialog_win);
		
		winningBox.setMessage(message);
		winningBox.show();

	}

	public void updatePlayerIndicator() {
		// update the player indicator
		if (playerIndicator != null) {
			String text;
			if (gamePieceStates.currentPlayer == Player.X) {
				text = getContext().getString(R.string.player_cross);
			} else {
				text = getContext().getString(R.string.player_circle);
			}
			playerIndicator.setText(text);

		}

	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// get canvas dimensions, though the board already knows them
		int cWidth = canvas.getWidth();
		int cHeight = canvas.getHeight();

		// draw the four grid lines
		// vertical ones
		canvas.drawLine(cWidth / 3, 0, cWidth / 3, cHeight - 1, gameBoardPaint);
		canvas.drawLine((cWidth / 3) * 2, 0, (cWidth / 3) * 2, cHeight - 1,
				gameBoardPaint);

		// horizontal ones
		canvas.drawLine(0, cHeight / 3, cWidth - 1, cHeight / 3, gameBoardPaint);
		canvas.drawLine(0, (cHeight / 3) * 2, cWidth - 1, (cHeight / 3) * 2,
				gameBoardPaint);
	}

	@Override
	public void onClick(View v) {
		GamePiece gp = (GamePiece) v;
		// find piece in array
		int x = 0;
		int y = 0;
		for (int i = 0; i < board_height; i++) {
			for (int k = 0; k < board_width; k++) {
				if (gamePieces[i][k] == gp) {
					y = i;
					x = k;
				}
			}
		}
		if (gamePieceStates.pieces[y][x] == Position.E && !gamePieceStates.isGameOver()) {
			Move circleMove;
			if (gamePieceStates.currentPlayer == Player.X) {
				gamePieceStates.pieces[y][x] = Position.X;
				gamePieceStates.currentPlayer = Player.O;
				if(!gamePieceStates.isGameOver()){
					circleMove = GameState.MinMax(gamePieceStates, Player.O);
					gamePieceStates.pieces[circleMove.row][circleMove.col] = Position.O;
					gamePieceStates.currentPlayer = Player.X;
				}
			}
			if (gamePieceStates.isGameOver()) {
				playVictory();
			}
		}
		// update board
		updateBoard();

	}
	

}

