package gumbyscout.tictactoe;

import gumbyscout.tictactoe.GameState.Position;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.Button;

public class GamePiece extends Button {

	protected Paint piecePaint;
	private final int GAME_PIECE_MARGIN = 20;

	private Position gamePieceState;

	public GamePiece(Context context) {
		super(context);

		// game pieces don't have text
		setText("");

		// game pieces don't have backgrounds
		setBackgroundColor(0x00000000);
		// test color
		//setBackgroundColor(0xFF00FFFF);

		// default paint to black
		piecePaint = new Paint();
		piecePaint.setARGB(255, 0, 0, 0);
		piecePaint.setStrokeWidth(15);

		// set default state
		gamePieceState = Position.E;

	}

	public Position getGamePieceState() {
		return gamePieceState;
	}

	public void setGamePieceState(Position pieces) {
		this.gamePieceState = pieces;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// get dimensions, I 'think' this is only the widget's surface
		int height = canvas.getHeight();
		int width = canvas.getWidth();

		if (gamePieceState == Position.X) {
			// negative slope line
			canvas.drawLine(0 + GAME_PIECE_MARGIN, 0 + GAME_PIECE_MARGIN, 
					width - (1 + GAME_PIECE_MARGIN), height - (1 + GAME_PIECE_MARGIN) , piecePaint);

			// positive slope line
			canvas.drawLine(0 + GAME_PIECE_MARGIN, height - (1 + GAME_PIECE_MARGIN),
					width - (1 + GAME_PIECE_MARGIN), 0 + GAME_PIECE_MARGIN, piecePaint);
		} else if (gamePieceState == Position.O) {
			canvas.drawCircle(width / 2, height / 2, (width / 2) - (1 + GAME_PIECE_MARGIN),
					piecePaint);
		}
	}

}
