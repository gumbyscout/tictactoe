package gumbyscout.tictactoe;

import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private LinearLayout mainLayout;
	private GameBoard gameboard;
	//private boolean isGameRunning;
	private final String LABEL_BUNDLE_STATES = "GamePieceStates";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//get the layout to manipulate it
		mainLayout = (LinearLayout)findViewById(R.id.layout_main);
		
		//set gravity for linear layout so that gameboard centers
		mainLayout.setGravity(Gravity.CENTER);
		
		//get device screen size
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int screen_height = metrics.heightPixels;
		int screen_width = metrics.widthPixels;
		
		//create player indicator
		TextView playerInd = new TextView(this);
		//playerInd.setTextColor(0xFFFFFFFF);
		
		//create and add the gameboard
		gameboard = new GameBoard(this);
		gameboard.setScreenDimensions(screen_width, screen_height);
		gameboard.populateBoard();
		gameboard.setGameBoardState(savedInstanceState);
		gameboard.setPlayerIndicator(playerInd);
		
		
		mainLayout.addView(gameboard);
		mainLayout.addView(playerInd);
		
		//isGameRunning = true;
		//main game loop
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(LABEL_BUNDLE_STATES, gameboard.getGameState());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {	
		if(item.getItemId() == R.id.menu_reset){
			gameboard.reset();
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

}
