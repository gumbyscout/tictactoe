package gumbyscout.tictactoe;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class GameState implements Serializable {
	private static final long serialVersionUID = -4250184720170601483L;

	static final int SIZE = 3;

	enum Position {
		E, X, O
	};

	enum Player {
		X, O
	};

	Player currentPlayer;
	Position pieces[][];

	public GameState() {
		pieces = new Position[SIZE][];
		for (int i = 0; i < SIZE; i++) {
			pieces[i] = new Position[SIZE];
			for (int j = 0; j < SIZE; j++) {
				pieces[i][j] = Position.E;
			}
		}
		currentPlayer = Player.X;
	}
	
	public GameState(GameState copy) {
		pieces = new Position[SIZE][];
		for (int i = 0; i < SIZE; i++) {
			pieces[i] = new Position[SIZE];
			for (int j = 0; j < SIZE; j++) {
				pieces[i][j] = copy.pieces[i][j];
			}
		}
		currentPlayer = copy.currentPlayer;
	}

	public GameState makeMove(Move move) {
		GameState newBoard = new GameState(this);

		if (currentPlayer == Player.X) {
			newBoard.pieces[move.row][move.col] = Position.X;
			newBoard.currentPlayer = Player.O;
		} else {
			newBoard.pieces[move.row][move.col] = Position.O;
			newBoard.currentPlayer = Player.X;
		}

		return newBoard;
	}

	public boolean isGameOver() {
		return isTie() || isWin(Player.X) || isWin(Player.O);
	}

	public List<Move> getMoves() {
		LinkedList<Move> list = new LinkedList<Move>();
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				if (pieces[row][col] == Position.E) {
					list.add(new Move(row, col));
				}
			}
		}
		return list;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public Position getPiece(Player player) {
		if (player == Player.X) {
			return Position.X;
		} else {
			return Position.O;
		}
	}

	boolean isWin(Player player) {
		return isDiagnolWin(player) || isHorizontalWin(player) || isVerticalWin(player);
	}

	boolean isHorizontalWin(Player player) {
		Position piece = getPiece(player);

		for (int row = 0; row < SIZE; row++) {
			int count = 0;
			for (int col = 0; col < SIZE; col++) {
				if (pieces[row][col] == piece) {
					count++;
				}
			}
			if (count == SIZE) {
				return true;
			}
		}
		return false;
	}

	boolean isVerticalWin(Player player) {
		Position piece = getPiece(player);

		for (int col = 0; col < SIZE; col++) {
			int count = 0;
			for (int row = 0; row < SIZE; row++) {
				if (pieces[row][col] == piece) {
					count++;
				}
			}
			if (count == SIZE) {
				return true;
			}
		}

		return false;
	}

	boolean isDiagnolWin(Player player) {
		Position piece = getPiece(player);

		int count = 0;
		for (int pos = 0; pos < SIZE; pos++) {
			if (pieces[pos][pos] == piece) {
				count++;
			}
		}
		if (count == SIZE) {
			return true;
		}

		// / diagonal
		count = 0;
		for (int pos = 0; pos < SIZE; pos++) {
			if (pieces[pos][SIZE - pos - 1] == piece) {
				count++;
			}
		}
		if (count == SIZE) {
			return true;
		}

		return false;
	}

	boolean isTie() {
		int count = 0;
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				if (pieces[row][col] != Position.E) {
					count++;
				}
			}
		}
		return count == (SIZE * SIZE);
	}
	
	public static Move MinMax (GameState board, Player player) {
		//Log.i("BOARD", "--------------------\n" + board.toString());
		
		if ( board.isGameOver()  ) { 
			Move result = board.evaluate(player);
			//Log.i("BOARD", "result=" + result.toString() + "\n\n");
			return result;
		}
	
		Move best = new Move(-1, -1);
		if ( board.getCurrentPlayer().equals(player) ) {
			best.score = Integer.MIN_VALUE;
		} else {
			best.score = Integer.MAX_VALUE;
		}
		
		for (Move move : board.getMoves()) {
			GameState newBoard = board.makeMove(move);
			Move nextMove = MinMax(newBoard, player);
			if ( board.getCurrentPlayer().equals(player)) {
				if ( nextMove.score > best.score ) { // max
					best.score = nextMove.score;
					best.col = move.col;
					best.row = move.row;
				}
			} else {
				if ( nextMove.score < best.score ) { // min
					best.score = nextMove.score;
					best.col = move.col;
					best.row = move.row;
				}
			}
		}
		return best;
	}
	


	public Move evaluate(Player player) {
		Player opponent = player == Player.X ? Player.O : Player.X;
		
		Move m = new Move(-1, -1);
		if (isWin(player) )
			m.score = 1;
		else if (isWin(opponent)) 
			m.score = -1;
		else
			m.score = 0;
		return m; 
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int row = 0; row < SIZE; row++) {
			for (int col = 0; col < SIZE; col++) {
				sb.append(pieces[row][col]);
				sb.append(' ');
			}
			sb.append('\n');
		}
		sb.append("current=" + currentPlayer + "\n");
		return sb.toString();
	}
}

class Move {
	int col;
	int row;
	int score;

	public Move(int row, int col) {
		this.col = col;
		this.row = row;
		score = Integer.MIN_VALUE;
	}

	@Override
	public String toString() {
		return "Move: row=" + row + " col=" + col + " score=" + score;
	}
}